# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

"""
INJ IO guardian module

This module provides functions for reading input files.

2016 - Christopher M. Biwer
2016-10-27 Keith Thorne - add optional toplevel_path
"""

import numpy
import os.path
import sys
import StringIO
import traceback
import gpstime
from inj_types import HardwareInjection

def read_schedule(schedule_path, toplevel_path='/'):
    """ Parses schedule file. Schedule file should be a space-delimited file
    with the following ordered columns: GPS start time, IFO list, INJECT state,
    observing mode, scale factor, and the path to the waveform file.

    The IFO list is a list of the IFOs to use in the form H1L1, H1, L1, etc.
    This should match what is returned by the Ezca.ifo instance method.

    The INJECT state should be one of the INJECT_*_ACTIVE guardian states that
    is a subclass of the _INJECT_STATE_ACTIVE state.

    The observing mode column should be 1 or 0. If there is a 1 the injection
    will be performed in observation mode and if there is a 0 the injection
    will be performed in commissioning mode.

    The scale factor should be a float. This is the overall factor a time
    series will be multiplied by before it is injected.

    The path to the waveform file is required and the filename should have the
    following format:
        IFO-TAG-GPS_START_TIME-DURATION.EXT

    Since each detector may have different time series to inject, we allow the
    user to use the {ifo} substring in the waveform_path column. So that the
    substring {ifo} is replaced with the IFO, eg. H1 or L1.

    Parameters
    ----------
    schedule_path: str
        Path to the schedule file.
    toplevel_path : str
        Top-level directory for waveform files (default /)

    Returns
    ----------
    inj_list: list
        A list where each element is an HardwareInjection instance.
    """

    # initialize empty list to store HardwareInjection
    hwinj_list = []

    # get the current GPS time
    current_gps_time = gpstime.gpsnow()

    # read lines of schedule file
    fp = open(schedule_path, "rb")
    lines = fp.readlines()
    fp.close()

    # loop over lines in schedule file
    for line in lines:

        # get line in schedule as a list of strings
        # assumes its a space-delimited line
        data = line.split()

        # parse line elements into variables
        i = 0
        schedule_time = float(data[i]); i += 1
        ifo_network = data[i]; i += 1
        schedule_state = data[i]; i += 1
        observation_mode = int(data[i]); i+= 1
        scale_factor = float(data[i]); i += 1
        waveform_path = data[i]; i += 1

        # add toplevel_path
        waveform_fullpath = os.path.join(toplevel_path,waveform_path)

        # add a new HardwareInjection to list if its in the future
        if schedule_time - current_gps_time > 0:
            hwinj = HardwareInjection(schedule_time, ifo_network, schedule_state,
                                      observation_mode, scale_factor,
                                      waveform_fullpath)
            hwinj_list.append(hwinj)

    return hwinj_list

def read_waveform(waveform_path, ftype="ascii"):
    """ Reads a waveform file. Only single-column ASCII files are
    supported for reading.

    Parameters
    ----------
    waveform_path: str
        Path to the waveform file.
    ftype: str
        Selects what method to use. Currently must be a string set to "ascii".

    Retuns
    ----------
    waveform: numpy.array
        Returns the time series as a numpy array.
    """

    # single-coulmn ASCII file reading
    if ftype == "ascii":

        # read single-column ASCII file with time series
        waveform = numpy.loadtxt(waveform_path)

    return waveform
